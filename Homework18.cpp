﻿#include <iostream>
#include <string>

using namespace std;

template <class T>
class Stack
{
private:
    T* pointer;
    int count;
public:
    ~Stack()
    {
        delete[] pointer;
    }

    void push(T str)
    {
        if (pointer == nullptr) {
            pointer = new T[1]{ str };
            count = 1;
        }
        else {
            T* newArray = new T[count + 1];
            newArray[0] = str;

            for (int i = 0; i < count; i++) {
                newArray[i + 1] = *(pointer + i);
            }

            delete[] pointer;
            pointer = newArray;
            count++;
        }
    }

    T pop()
    {
        if (pointer == nullptr || count == 0) {
            return "";
        }
        else {
            T result = *pointer;
            T* newArray = new T[count - 1];

            for (int i = 1; i < count; i++) {
                newArray[i - 1] = *(pointer + i);
            }

            delete[] pointer;
            pointer = newArray;
            count--;

            return result;
        }
    }
};

int main()
{
    Stack<string>* stack = new Stack<string>();

    stack->push("!");
    stack->push("World");
    stack->push("Hello");

    std::cout << stack->pop() << " ";
    std::cout << stack->pop() << "";
    std::cout << stack->pop() << "\n";
    return 0;
}
